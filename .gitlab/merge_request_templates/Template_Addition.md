## Problem Statement

<!-- What does this MR aim to solve, what new features does this add?> -->

## Links

<!-- Any links necessary -->

## Final QA Checks.
- [ ] Included a link to the issue for this MR.
- [ ] Included a link to the MR adding this template to the pipeline builder.
- [ ] Included a link to the new container supporting this template. (If Applicable)
- [ ] Included a link to a test pipeline showing the template works.
- [ ] Included relevant documentation changes.



