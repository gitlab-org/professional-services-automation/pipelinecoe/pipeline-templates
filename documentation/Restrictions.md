# Introduction

Usage of the DevSecOps Governance Framework (DGF) pattern and style of development comes with some restrictions. There are things you can and cannot do while using this pattern. We'll try to list each of these and be upfront about them. But we can't anticipate every customer's need, so you may have a situation or pattern the DGF is incompatible with.

## Known Restrictions

- **Premium License, or higher** - The DGF leverages functionality that is only available with a premium license or higher. There is optional functionality that requires an Ultimate license.