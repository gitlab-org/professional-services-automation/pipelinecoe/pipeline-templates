![Designs/CompliancePipeline.png](Designs/CompliancePipeline.png)

# Introduction

Pictured above is a diagram about a pipeline based on the compliance framework. It is divided into three sections; Compliance, General Purpose, and Secure. Each of these three sections have their own runners and their own environments. Application & Deployment Credentials are only exposed to runners on an as-needed basis, and only in a secure environment. Any calls to deploy to live environments must occur from a secure runner/environment.

Each job that is executed as part of this pipeline is ran in one of those three sections (Compliance, General Purpose, and Secure). The jobs derive their capabilities from the runner. The jobs cannot perform any commands or actions with any higher privlege than the runner gives them.


# Compliance

The compliance stages of this pipeline are compulsory to Application Teams. The Application Teams are not able to modify or manipulate anything included inside of the Compliance Stages. This is handled via the use of [Compliance Frameworks](https://docs.gitlab.com/ee/user/project/settings/index.html#compliance-frameworks). 

## Preflight Stage

The purpose of the preflight stage is to conduct any checks for compliance before the pipeline starts. Typically this section is utilized to check variables, ensure nothing in the project's .gitlab-ci.yml file is outside of compliance, and any other "preflight checks."

## Compliance Stage

The compliance stage sits as the gatekeeper between General Purpose environments, and Secure Environments. 

This stage is designed to execute:
- all security scans
- any compliance scans
- bill of material auditing
- chain of custody audits. 

## Live Compliance

Some compliance features require a live environment to test against. Most notably this is the [DAST](https://docs.gitlab.com/ee/user/application_security/dast/), [API Fuzzing](https://docs.gitlab.com/ee/user/application_security/api_fuzzing/) and [Kubernetes Cluster Image Scanning](https://docs.gitlab.com/ee/user/application_security/cluster_image_scanning/).  Any compliance tasks that require the application to be live and accessible, happen in this stage.

This stage comes after the Staging stage, so that we know we have a live environment to check against. This stage can and will block any/all application deployments.

# General Purpose

The General Purpose stages house all the jobs and tasks needed to get a project ready to be deployed to an environment. These stages are the areas in which Application teams can run any code/tools they desire inside of the containers/jobs. Because these runners have no access to any secrets or any protected environments. Thus, any unsafe code or tools will have a very small blast radius and not be able to affect anything in other environments.

## Build Stage

The jobs in this stage exist to compile/assemble the project's artifacts.

## Test Stage

The jobs in this stage exist to test all of the project's source code, and artifacts. Things like unit tests exist here.

## Package Stage

This stage exists to run any jobs that package the project's artifacts. Docker builds, Nexus packages, etc exist here.

# Secure

The secure stages exist to run jobs on runners who have access to secrets and environments in which they can deploy items. 

## Staging / Production

For the jobs in this stage, they all run on privleged runners. Thus, these jobs have the ability to deploy to live environments and access secrets. This is why these jobs occur after the initial compliance stages. From here you can deploy your application.
