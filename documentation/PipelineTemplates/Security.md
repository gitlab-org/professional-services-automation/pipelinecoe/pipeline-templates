# Security 

Security jobs are part of the `Security.gitlab-ci.yml` template, which will be added/extended in your pipeline through the `Application.gitlab-ci.yml`.

## Nexus IQ

### .nexus-scan

This job is used to invoke the Sonatype Nexus IQ Scan. IQ Server configurations need to be in place for this job to function. For more information, see [sonatype/gitlab-nexus-iq-pipeline](https://hub.docker.com/r/sonatype/gitlab-nexus-iq-pipeline). 

**Usage**

```yaml
nexus-scan:
  extends: .nexus-scan
  variables:
    OPTIONS: ""
    DIRECTORY: ""
```

**Image**: [sonatype/gitlab-nexus-iq-pipeline:1.1](https://hub.docker.com/r/sonatype/gitlab-nexus-iq-pipeline)

**Variables**

| Name | Default | Description |
| --- | --- | --- |
|OPTIONS|''|Used to specify any Nexus IQ Options.|
|DIRECTORY|''|Used to specify the directory the scan occurs in.|

---

## Twistlock

### .twistlock-scan

This job is used to invoke a twistlock container scan. This will build a container image, then set up Twistlock to connect to the docker daemon to scan that image. Results reported in the twistlock dashboard. Prisma Cloud Compute configurations need to be in place for this job to function. For more information, see [twistlock/sample-code/CI/GitLab](https://github.com/twistlock/sample-code/tree/master/CI/GitLab).

**Usage**

```yaml
twistlock-scan:
  extends: .twistlock-scan
  variables:
    IMAGE_NAME: "$CI_PROJECT_PATH:$CI_COMMIT_SHA"
    PCC_USER: "$PCC_USER"
    PCC_PASS: "$PCC_PASS"
    PCC_CONSOLE_URL: "$PCC_CONSOLE_URL"
```

**Image**: [docker:19.03](https://hub.docker.com/_/docker)

**Variables**

| Name | Default | Description |
| --- | --- | --- |
|IMAGE_NAME|'$CI_PROJECT_PATH:$CI_COMMIT_SHA'|Used to specify what image to scan.|
|PCC_USER|''|The username to login to the twistlock console.|
|PCC_PASS|''|The token to login to the twistlock console.|
|PCC_CONSOLE_URL|''|The twistlock console URL.|

---

## Fortify

### .fortify-scan

The Fortify CI Tools simplify integration of Fortify static application security testing (SAST) into DevSecOps pipelines that use configurable runners to execute CI/CD workflows. This job is used to define the fortify scan parameters. It is not meant to be invoked directly. For more information, see [fortifydocker/fortify-ci-tools](https://hub.docker.com/r/fortifydocker/fortify-ci-tools).

**Usage**

```yaml
fortify-scan:
  extends: .fortify-scan
  variables:
    APPLICATION_PACKAGE: ""
    FOD_URL: ""
    FOD_API_URL: ""
    FOD_UPLOADER_OPTS: ""
    FOD_NOTES: "Triggered by Gitlab Pipeline IID $CI_PIPELINE_IID: $CI_PIPELINE_URL"
    FOD_USERNAME: ""
    FOD_PAT: ""
```

**Image**: [fortifydocker/fortify-ci-tools:latest](https://hub.docker.com/r/fortifydocker/fortify-ci-tools)

**Variables**

| Name | Default | Description |
| --- | --- | --- |
|APPLICATION_PACKAGE|""|Specify the output file name. The file extension must be *.zip.|
|FOD_URL|""|Domain URL.|
|FOD_API_URL|""|API root URL.|
|FOD_UPLOADER_OPTS|""|Other options, as needed.|
|FOD_NOTES|"Triggered by Gitlab Pipeline: $CI_PIPELINE_URL"|The notes about the scan.|
|FOD_USERNAME|""|User name.|
|FOD_PAT|""|User password.|

### .fortify-maven-scan

This job builds upon the base `.fortify-scan` to enact a scan against a Maven target.

**Usage**

```yaml
fortify-maven-scan:
  extends: .fortify-maven-scan
  variables:
    APPLICATION_PACKAGE: ""
    FOD_URL: ""
    FOD_API_URL: ""
    FOD_UPLOADER_OPTS: ""
    FOD_NOTES: "Triggered by Gitlab Pipeline IID $CI_PIPELINE_IID: $CI_PIPELINE_URL"
    FOD_USERNAME: ""
    FOD_PAT: ""
```

**Image**: [.fortify-scan image](#fortify-scan)

**Variables**

| Name | Default | Description |
| --- | --- | --- |
|PACKAGE_OPTS|"-bt mvn"|Specify the build tool name used for the project.|

### .fortify-gradle-scan

This job builds upon the base `.fortify-scan` to enact a scan against a Gradle target.

**Usage**

```yaml
fortify-gradle-scan:
  extends: .fortify-gradle-scan
  variables:
    APPLICATION_PACKAGE: ""
    FOD_URL: ""
    FOD_API_URL: ""
    FOD_UPLOADER_OPTS: ""
    FOD_NOTES: "Triggered by Gitlab Pipeline IID $CI_PIPELINE_IID: $CI_PIPELINE_URL"
    FOD_USERNAME: ""
    FOD_PAT: ""
```

**Image**: [.fortify-scan image](#fortify-scan)

**Variables**

| Name | Default | Description |
| --- | --- | --- |
|PACKAGE_OPTS|"-bt gradle"|Specify the build tool name used for the project.|

---

## SonarQube

### .sonar-scan

This job is used to define the sonarqube scan parameters. It is not meant to be invoked directly. SonarQube server configurations need to be in place for this job to function. For more information, see [SonarQube | GitLab CI/CD](https://docs.sonarqube.org/8.5/analysis/gitlab-cicd/).

**Image**: *none*

**Variables**

| Name | Default | Description |
| --- | --- | --- |
|SONAR_USER_HOME|'${CI_PROJECT_DIR}/.sonar'|Sonarqube's home directory. Stored in a cache by default.|
|GIT_DEPTH|'0'|Git history truncated to the specified number of commits|

### .sonar-maven-scan

This job builds upon the base `.sonar-scan` to enact a scan against a Maven target.

**Usage**

```yaml
sonar-maven-scan:
  extends: .sonar-maven-scan
  variables:
    SONAR_USER_HOME: "${CI_PROJECT_DIR}/.sonar"
    GIT_DEPTH: "0"
```

**Image**: *custom-registry*/maven-container:latest

**No Variables**

### .sonar-gradle-scan

This job builds upon the base `.sonar-scan` to enact a scan against a Gradle target.

**Usage**

```yaml
sonar-gradle-scan:
  extends: .sonar-gradle-scan
  variables:
    SONAR_USER_HOME: "${CI_PROJECT_DIR}/.sonar"
    GIT_DEPTH: "0"
```

**Image**: *custom-registry*/gradle-container:latest

**No Variables**

### .sonar-cli-scan

This job builds upon the base `.sonar-scan` to enact a scan against a CLI target.

**Usage**

```yaml
sonar-cli-scan:
  extends: .sonar-cli-scan
  variables:
    SONAR_USER_HOME: "${CI_PROJECT_DIR}/.sonar"
    GIT_DEPTH: "0"
```

**Image**: [sonarsource/sonar-scanner-cli:latest](https://hub.docker.com/r/sonarsource/sonar-scanner-cli)

**No Variables**

---

## Tern

### .tern-scan

This job invokes a TERN Scan against a Docker container. It produces an output file that is a Software Bill of Materials (SBOM). This file is then archived for storage. For more information, see [TERN](https://github.com/tern-tools/tern#readme).

**Usage**

```yaml
tern-scan:
  extends: .tern-scan
  variables:
    REGISTRY: $CI_REGISTRY
    PATH: ''
```

**Image**: *custom-registry*/tern-container:latest

**Variables**

| Name | Default | Description |
| --- | --- | --- |
|REGISTRY|'$CI_REGISTRY'|Custom registry if not using GitLab's container registry.|
|PATH|''|Path to the image if not using GitLab's container registry|
