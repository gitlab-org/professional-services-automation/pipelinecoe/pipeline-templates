# Governance

> :warning: Compliance Framework Pipelines are only available with an **Ultimate license**

The Governance Template is the file used for [GitLab Compliance Pipelines](https://docs.gitlab.com/ee/user/project/settings/index.html#compliance-pipeline-configuration) in support of the [Compliance Frameworks](https://docs.gitlab.com/ee/user/project/settings/index.html#compliance-frameworks).

This file should contain no jobs. It's purpose is to be a 'meta include', i.e. all **Immutable** Global Variables should be defined in this file. In addition, any and all files that you want to include in every pipeline run should be included here at the top. 

The purpose of this file is to enforce rules, jobs, and variables for a pipeline.  You force this file to be included by defining it in the Group Settings -> Repository -> Compliance Framework section. Then by including it against a project via Project Settings -> Repository -> Compliance Pipeline. 

Once set to be enforced at the Group and Project level. This file will be the file GitLab CI processes and runs first. Everything set and defined in this file will be immutable and unable to be changed or overwritten by the development teams. This file will invoke the `.gitlab-ci.yml` file in a team's repository; there the team can define any number of jobs or variables. However, what's defined in this file will take precedent over anything defined in the individual projects.

By default, this file will include the entire GitLab Security Suite, and you can override any settings of the GitLab Security Suite in `SecurityOverrides.gitlab-ci.yml`. For example:

```yaml
license_scanning:
    stage: .pre
```

The stages defined in this file will standardize the stages required by the individual projects. They will not be able to have other stages beyond the ones listed here. If you do not want that restriction, you can remove the stages section, but you will have to build checks to ensure certain stages are called to enfore the template compliance.

For more information, see [Applicaiton Security](https://docs.gitlab.com/ee/user/application_security/).
