# Implementation Guide

This guide is to help you implement the DevSecOps Governance Framework (DGF), formerly known as the pipeline COE, inside of your organization.

There are 2 options available to get the DGF into your GitLab instance.

- Package (Tarball)
- Container Image

The automation has been created to make the building of the DGF a little easier. It is designed to be usable for connected and air-gapped environments.

## Prerequisites

1. A [Personal Access Token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html) with `read_api` and `read_registry` scopes for [GitLab.com](gitlab.com). `$GITLAB_TOKEN` will be used in this guide.
1. A user with `Can create group` access in target GitLab environment. More info on [Permissions and roles](https://docs.gitlab.com/ee/user/permissions.html).
1. A [Personal Access Token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html) for above user that has `api` and `write_repository` scopes in target GitLab environment. `$USER_NAME` and `$TOKEN` will be used in this guide.
1. *Optional* - If deploying to a parent group, please make sure [shared runners are enabled](https://docs.gitlab.com/ee/ci/runners/runners_scope.html#enable-shared-runners-for-a-group), or that subgroups can override the setting
1. If running the script, you will need to have [git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git) and [jq](https://stedolan.github.io/jq/download/) installed on the machine runnning the scripts.
1. A GitLab runner will be needed to build the containers after the Pipeline COE is in place.

## Understanding the Files and Scripts

Below is a description of the files and scripts that are part of deploying the Pipeline COE. The deploy the Pipeline COE see [Deploying the Pipeline COE](#deploying-the-pipeline-coe). These files are also being used inside of the container image, but are not accessible directly.

> **NOTE** If you require commit signing with GPG Keys, please use the package option.

### DGF Container Image

The DGF (formerly known as pipeline COE) Container is stored in the Container Registry in the Pipeline Templates project. You can get the container via docker pull. There are 2 container images, the open source version and the full feature version. The open source container image can be pulled by anyone while the full-feature container image requires access to the GitLab Professional Services delivery kit.

- Open Source
  - Container Registry URL: https://gitlab.com/gitlab-org/professional-services-automation/pipelinecoe/pipeline-templates/container_registry/
  - Container Image Path: 
  ```bash
  CONTAINER_IMAGE="registry.gitlab.com/gitlab-org/professional-services-automation/pipelinecoe/pipeline-templates/pipelinecoe:latest"
  ```
- Full Feature
  - Container Registry URL: https://gitlab.com/gitlab-org/professional-services-automation/delivery-kits/pipeline-coe/container_registry/
  - Container Image Path: 
  ```bash
  CONTAINER_IMAGE="registry.gitlab.com/gitlab-org/professional-services-automation/delivery-kits/pipeline-coe/pipelinecoe:latest"
  ```

### DGF Package

The DGF is stored in the Package Registry and is zipped in a file named PipelineCOE.tar.gz in the Pipeline Templates project. You can get the package manually or via API. There are 2 packages, the open source version and the full feature version. The open source package can be pulled by anyone while the full-feature package requires access to the GitLab Professional Services delivery kit.

- Open Source
  - Package Registry URL: https://gitlab.com/gitlab-org/professional-services-automation/pipelinecoe/pipeline-templates/-/packages/
  - Package Path: 
  ```bash
  PACKAGE_PATH="https://gitlab.com/api/v4/projects/gitlab-org%2Fprofessional-services-automation%2Fpipelinecoe%2Fpipeline-templates/packages/generic/PipelineCOE/1.0.0/PipelineCOE.tar.gz
  ```
- Full Feature
  - Package Registry URL: https://gitlab.com/gitlab-org/professional-services-automation/delivery-kits/pipeline-coe/-/packages/
  - Package Path: 
  ```bash
  PACKAGE_PATH="https://gitlab.com/api/v4/projects/gitlab-org%2Fprofessional-services-automation%2Fdelivery-kits%2Fpipeline-coe/packages/generic/PipelineCOE/1.0.0/PipelineCOE.tar.gz"
  ```

### Configuration File

We will use the config.json file to pass configuration requirements. It is located in the Pipeline Templates project inside the Pipeline COE Group. [Pipeline COE > Pipeline Templates > config.json](https://gitlab.com/gitlab-org/professional-services-automation/pipelinecoe/pipeline-templates/-/blob/main/config.json)

#### Example Configuration File

```json
{
    "gitlaburl": "https://gitlab.example.com",
    "container_path": "registry.gitlab.example.com/pipelinecoe/containers",
    "base_container_path": "registry.access.redhat.com/ubi8/ubi:latest",
    "buildah_container_path": "registry.access.redhat.com/ubi8/buildah:latest",
    "parent_id": "",
    "tags": "",
    "registry": "",
    "registry_user": "",
    "registry_password": "",
    "image_path": "",
    "visibility": "",
    "git_user_name": "",
    "git_user_email": "",
    "git_user_gpgsigningkey": "",
    "custom_commit_message": "",
    "enable_group_runners": "true",
    "enable_shared_runners": "true"
}
```
| Key | Value |
| - | - |
| **gitlaburl** | The URL to the instance |
| **container_path** | The path to the `containers` group. If the DGF is being created under another group (parent group), it is the path to the parent group. For example, DGF's parent group is Group 1, then the url will be `registry.gitlab.example.com/group-1/pipelinecoe/containers` where `registry.gitlab.example.com` is the DNS entry for GitLab Registry. |
| **base_container_path** | Location of the UBI8 latest image. Can be modified for a customer base container. |
| **buildah_container_path** | Location of the `Buildah` image. Can be modified for a customer base container. |
| **parent_id** | The parent id of the project that you want to nest the DGF under. If you creating at the instance level, set to `""`, otherwise provide the group id. |
| **tags** | GitLab Runner tags to be added to the gitlab-ci.yml files to allow runners to pick up jobs. If you do not want to use tags, leave as `""`. If you want tags, add them comma delimited, i.e. `"tags": "dev,test,prod"`. |
| **registry** | Variable that contains the path to the container registry. If you are using the GitLab Container Registry, set to `""`. If you are using another registry, provide a variable i.e `"$CUSTOM_REGISTRY"`. |
| **registry_user** | Variable that contains the user to access the contaner registry. If you are using the GitLab Container Registry, set to `""`. If you are using another registry, provide a variable i.e `"$CUSTOM_REGISTRY_USER"`. |
| **registry_password** | Variable that contains the password/token to access the container registry. If you are using the GitLab Container Registry, set to `""`. If you are using another registry, provide a variable i.e `"$CUSTOM_REGISTRY_PASSWORD"`. |
| **image_path** | Variable that contains the path fro the image/images in the container registry. If you are using the GitLab Container Registry, set to `""`. If you are using another registry, provide a variable i.e `"$CUSTOM_IMAGE_PATH"`. |
| **visibility** | Used to set the visibility of the groups and projects, should be *public*, *internal*, or *private*. If you want the default visibility of the parent group, set to `""`. If you are not nesting under a parent group and you set to `""`, visibility will be set to the GitLab instance default. |
| **git_user_name** | Git user name to be passed for custom git config user.name. If you leave as default, user.name is `gitlab`. |
| **git_user_email** | Git user email to be passed for custom git config user.email. If you leave as default, user.email is `<>`. |
| **git_user_gpgsigningkey** | Git user GPG key to be passed for git config user.signingkey. If you leave as default, gpg signing is set to `false`. |
| **custom_commit_message** | Git commit message to be passed for git commit. If you leave as default, commit message is `initial commit`.  |
| **enable_group_runners** | Enables or disables group runners for projects. Allowed values: `true`, `false`, `""` |
| **enable_shared_runners** | Enables or disables shared runners for projects. Allowed values: `true`, `false`, `""` |

> [!NOTE]
> The default configurations for base_container_path and buildah_container_path reach out to the internet. If the container is unable to reach outside, please change the configuraiton to a registry that it can reach.

### Deploy Script

The script that will be used is deploycoe and located in the bin folder of the Pipeline Templates project inside the DGF Group, formerly known as Pipeline COE. [DevSecOps Governance Framework > Pipeline Templates > bin > deploycoe](https://gitlab.com/gitlab-org/professional-services-automation/pipelinecoe/pipeline-templates/-/blob/main/bin/deploycoe). This script will build out the group and project structure in the target environment. Below are the processes completed by this script:

1. Verification that you updated the config.json file
1. Update local files with data from config.json
1. Update gitlab-ci.yml files to proper path
1. Creates the Pipeline COE group in the target environment
1. Creates Projects in the Pipeline COE group and pushes repos
1. Creates sub groups and respective projects and pushes repos to projects

The usage for the script is `./deploycoe -d directory -u $USER_NAME -t $TOKEN [--skip-verification]`

The following are the variables leveraged by the script via flags:

- **--directory, -d** - The local directory to store the export
- **--username, -u** - The username of the person that has access to create groups and projects on the target instance
- **--token, -t** - The Access Token of the that has api persmisson on the target instance
- **--skip-verification** - *OPTIONAL* - Skip verification that the config.json file was updated

## Deploying the Pipeline COE

You can deploy the Pipeline COE with the [container image](#deploying-with-the-container-image) or the [package](#deploying-with-the-package).

### Deploying with the container image

1. Login to the Gitlab Container Registry with your username and the personal access token you created with `read_api` and `read_registry` scopes.

    ```bash
    docker login registry.gitlab.com
    ```

1. Pull the [desired container image](#pipeline-coe-container-image) down locally.

    ```bash
    # Set $CONTAINER_IMAGE with the Container Image Path
    docker pull $CONTAINER_IMAGE
    ```

1. Create a file name `config.json` and edit the file to add the [required values](#example-configuration-file).

    ```bash
    vim config.json
    ```

1. Execute via the `docker run` command below. The command will replace the config.json file in the `pipelinecoe` directory with the one create in the previous step. The flag usage is the same as annotated in [Deploy Script](#deploy-script).

    ```bash
    # $CONTAINER_IMAGE should be set with Container Image Path from previous command
    docker run --rm -v $PWD/config.json:/opt/pipelinecoe/pipeline-templates/config.json --name pipelinecoe $CONTAINER_IMAGE -d /opt -u $USER_NAME -t $TOKEN --skip-verification
    ```

    > [!WARNING]
    > Do not remove or change the `-d /opt` flag as it is pointing to the proper directory in the container.

    > [!WARNING]
    > Do not remove the `--skip-verification` flag as it is need for the container to process the scripts.

1. When complete, all groups and projects will be created and ready for use.

1. Clean up the environment.

    ```bash
    # Remove the Image
    docker rmi $CONTAINER_IMAGE

    # Clear Variables
    unset USER_NAME
    unset TOKEN

    # Remove configuration file
    rm -rf config.json
    ```

### Deploying with the package

1. Navigate to the desired local direcotry and download the pipelinecoe package. You can manually download the file by navigating to the desired Pipeline COE [Package Repository](#pipeline-coe-package). Once in the repository click on the `PipelineCOE` package and then click on the `PipelineCOE.tar.gz` file. If you prefer to download the package via api, use this curl command:

    ```bash
    # Replace $PACKAGE_PATH with the Package Path

    # For open source package
    curl -L -o PipelineCOE.tar.gz "$PACKAGE_PATH"

    # For full feature package
    curl --header "PRIVATE-TOKEN: $GITLAB_TOKEN" -L -o PipelineCOE.tar.gz "$PACKAGE_PATH"
    ```

1. Extract the tarball in the directory. This will create a folder called `pipelinecoe` in the directory.

    ```bash
    tar -xzf PipelineCOE.tar.gz
    ```

1. We recommend you copy the `config.json` to the local directory for editing, then copy it back to the required location. Navigate to the pipeline-templates directory and edit the config.json file accordingly.

    ```bash
    cp /usr/local/pipelinecoe/pipeline-templates/config.json .
    ```

1. Edit `config.json` to modify the [required values](#example-configuration-file).

    ```bash
    vim config.json
    ```

1. Copy the `config.json` back to where it needs to be to execute.

    ```bash
    cp ./config.json /usr/local/pipelinecoe/pipeline-templates/config.json
    ```

1. *Optional* - You can add the certificates for the Container Base and the Buildah Container.

    - If you want to upload certificates for your container base, add the certificates to `/usr/local/pipelinecoe/containers/container-base/certs`
    - If you want to upload certificates for your container base, add the certificates to `/usr/local/pipelinecoe/containers/buildah-container/certs`

1. Add execute permissions to the `deploycoe` script.

    ```bash
    chmod +x ./pipelinecoe/pipeline-templates/bin/deploycoe
    ```

1. Execute the `deploycoe` script passing the required variables. In this example, the `$PWD` is the directory where the `pipelinecoe` folder redsides, `/usr/local`. Make sure to replace `$USER_NAME` and `$TOKEN`, or set them as variables.

    ```bash
    ./pipelinecoe/pipeline-templates/bin/deploycoe -d $PWD -u "$USER_NAME" -t "$TOKEN"
    ```

1. When complete, all groups and projects will be created and ready for use.

1. Clean up the environment.

    ```bash
    # Remove the package
    rm -rf PipelineCOE.tar.gz

    # Remove the workspace
    rm -rf ./pipelinecoe

    # Clear Variables
    unset USER_NAME
    unset TOKEN
    ```

## Building Containers

1. Ensure you have a runner available to projects in the Pipeline COE. See [Registering Runners](https://docs.gitlab.com/runner/register/).
1. Create an Access Token at the Pipeline COE or Container group level with read_api scope. 
1. Create a masked variable named "READ_TOKEN" with the Access Token. It will be used to make an API call to get all container project ids.
1. *Optional* - Update certificates for "Container Base" and "Buildah Container" in the `certs` directory for each respective project.
`Pipeline COE > Containers > Example Container > certs`
1. Run pipeline for "Container Base". The Container Base project will trigger all other containers to build - see [Run a pipeline manually](https://docs.gitlab.com/ee/ci/pipelines/#run-a-pipeline-manually).
`Pipeline COE > Containers > Example Container > CI/CD > Pipelines > Run pipeline`

> [!ATTENTION]
> These pipelines may succeed "as-is", including on the initial push during deployment, depending on the permissions of the user that created the pipeline.

## Setting Templates

You can set templates at the [instance](https://docs.gitlab.com/ee/user/admin_area/custom_project_templates.html) or [group](https://docs.gitlab.com/ee/user/group/custom_project_templates.html) level.

*Instance Level Template Settings*

![Instance Level Templates](img/instance-level-templates.png)

*Group Level Template Settings*

![Group Level Templates](img/group-level-templates.png)
